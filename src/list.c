/*
 
   Copyright 2019 RNGHACK

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

struct LIST {
         s32        len;
  struct LIST_ITEM  *first;
  struct LIST_ITEM  *last;
  struct LIST_ITEM  *iter;
};


struct LIST_ITEM {
  struct LIST       *list;
  struct LIST_ITEM  *next;
         void       *item;
};

// @mem_new
struct LIST *list$make() {
  struct LIST *list;

  list = malloc(sizeof(struct LIST));

  // @todo verify malloc successfull 
  list->len = 0;
  list->first = NULL;
  list->last  = NULL;

  list->iter  = malloc(sizeof(struct LIST_ITEM));
  list->iter->next  = NULL;
  list->iter->list  = list; 

  return list;
}


// @mem_new
struct LIST_ITEM *list$append(struct LIST *list, void *item) {
  struct LIST_ITEM *list_item;

  list_item = malloc(sizeof(struct LIST_ITEM));

  // @TODO: check that malloc worked... else panic

  // setup the list item
  list_item->list = list;
  list_item->next = NULL;
  list_item->item = item;

  if(list->len == 0) {
    list->first      = list_item; 
    list->iter->next = list_item;
    list->last       = list_item;
  } else {
    list->last->next = list_item;
    list->last       = list_item;
  }
  
  list->len ++; 
  return list_item; 
}


void *list$first(struct LIST *list) {
  return list->first;
}

void *list$rest(struct LIST *list) {
  return list->first->next;
}


bool list$empty(struct LIST *list) {
  if(list->len <= 0) {
    return true;
  }
  return false;
}


u32 list$size(struct LIST *list) {
  
  return list->len;
}


struct LIST_ITEM *list$next(struct LIST_ITEM *list_item) {
  return list_item->next;
}


bool list$has_next(struct LIST_ITEM *list_item) {
  if(list_item == NULL) {
    return false;
  }
  return true;
}


void *list$get(struct LIST_ITEM *list_item) {
  return list_item->item;
} 


void list$clear(struct LIST *list) {
  if(list$empty(list) == true) return;

  struct LIST_ITEM *li      = list$first(list);
  struct LIST_ITEM *li_prev = NULL;

  do {
    if(li_prev != NULL) {
      free(li_prev);
    }
    li_prev = li;
    li = list$next(li);
       
  } while(list$has_next(li));

  list->first      = NULL;
  list->last       = NULL; 
  list->iter->next = NULL;

  free(li_prev); 
  list->len = 0;
}


void list$free(struct LIST *list) {
  list$clear(list);
  free(list);

}


void list$push(struct LIST *list, void *item) { 
  struct LIST_ITEM *list_item;

  list_item = malloc(sizeof(struct LIST_ITEM));

  list_item->item  = item; 
  list_item->next  = list->first; 
  list->first      = list_item; 
  list->iter->next = list_item;

  list->len ++;
}


void *list$pop(struct LIST *list) {
  if(list->len == 0) {
    return NULL;
  }
  struct LIST_ITEM *list_item;
  void   *item;

  list_item = list->first;
  item      = list_item->item;

  list->first = list_item->next;
  list->iter->next = list->first; 

  free(list_item);
  list->len --;

  return item;

}


void list$copy(struct LIST *src, struct LIST *dest) {
  if(list$empty(src) == true) {
    return;
  } 

  // TODO should we empty the destination list if it has items?

  struct LIST_ITEM *src_item       = list$first(src);
  struct LIST_ITEM *new_item       = NULL;
  struct LIST_ITEM *prev_new_item;

  do {
    prev_new_item  =  new_item; 

    new_item       =  malloc(sizeof(struct LIST_ITEM));
    new_item->item =  src_item->item; 
    new_item->list =  dest;

    if(prev_new_item == NULL) {
      dest->first = new_item;
    } else {
      prev_new_item->next  = new_item; 
    } 

    src_item = list$next(src_item);
      
  } while(list$has_next(src_item));

  new_item->next = NULL;
  dest->iter->next = dest->first;
  dest->last = new_item; 
  dest->len  = src->len; 
} 



bool list$iter(struct LIST_ITEM **li, void **item) { 

  struct LIST_ITEM *current = *li;
  struct LIST_ITEM *next    = current->next;

  if(next == NULL) {
    *item = NULL;
    return false;
  }

  *item = next->item;
  *li   = next;

  return true;
} 


void list$insert_after(struct LIST_ITEM *li, void *item) {
  struct LIST_ITEM *new_li =  malloc(sizeof(struct LIST_ITEM));

  new_li->item = item;
  new_li->list = li->list;

  new_li->next = li->next;
  li->next     = new_li;

  if(li->list->last == li) {
   li->list->last = new_li;
  } 
}


// TESTS =============================================================

u32 list_test$iterator() {
  // TODO test for empty characters
  
  struct STRING hello; 
  struct STRING world;

  string$from_c_string(&hello, "hello");
  string$from_c_string(&world, "world");

  struct LIST   *list    = list$make();

  list$append(list, &hello);
  list$append(list, &world);

  struct STRING    *str;
  struct LIST_ITEM *iter = list->iter;

  u32 i = 0;

  // TODO when we deprecate while(list$iter(&list->iter, (void **)&str)) { 

  while(list$iter(&iter, (void **)&str)) { 
    i++;
  }  

  if(i == 2) {
    return 0;
  }
  return 1;
}



u32 list_test$copy() {
  struct STRING hello; 
  struct STRING world;

  string$from_c_string(&hello, "hello");
  string$from_c_string(&world, "world");

  struct LIST   *src    = list$make();
  struct LIST   *dest   = list$make();

  list$append(src, &hello);
  list$append(src, &world);

  list$copy(src, dest);

  struct LIST_ITEM *li;
  struct STRING    *str; 
  u32 i = 0;
  li = list$first(dest);
  do {
    str = list$get(li); 

    if(i==0) {
      if(string$equals(str, &hello) == false) {
        printf("expected hello\n");
        return 1;
      }
    } 
    if(i==1) {
      if(string$equals(str, &world) == false) {
        printf("expected world\n");
        return 1;
      }
    } 
    
    i++;
    li = list$next(li);
  } while(list$has_next(li)); 

  if(list$size(src) == list$size(dest)) {
    return 0;
  }

  printf("list size does not match!");

  return 1;
}


u32 list_test$push() {
  struct LIST *list = list$make();

  char *hi = "Hello world";

  list$push(list, hi);

  if(list->len != 1) {
    printf("list count not correct, expected 1 and got %d", list->len);
    return 1;
  }

  if(list->first->item != hi) {
    printf("did not find item pushed as the first item"); 
    return 1;
  }

  return 0;
}


u32 list_test$pop() {
  struct LIST *list = list$make();

  char *hi = "Hello world";

  list$push(list, hi);
  char *item = list$pop(list);

  if(list->len != 0) {
    printf("list count not correct after pop, expected 0 and got %d", list->len);
    return 1;
  }

  if(item != hi) {
    printf("item popped was not the item pushed"); 
    return 1;
  }

  return 0; 
}


u32 list_test$make() {

  struct LIST *list = list$make();

  if(list <= 0) {
    printf("***FAILED*** could not create memory for list\n");
    printf("address: %p\n", list);
    return 1;
  }

  s32 list_size = list->len;

  if(list_size != 0) {
    printf("***FAILED*** list not initialised correctly, size != 0\n");
    printf("list_size: %d\n", list_size);
  }
  // @todo check of struct items
  free(list); 
  return 0;  
}


u32 list_test$append() {
  char* c_str = "hello";
  
  struct STRING str; 
  string$from_c_string(&str, c_str);

  struct LIST *list = list$make();

  list$append(list, &str);

  // @TODO check list length and list_item and item

  free(list);
  return 0;  
}



u32 list_test$iterate() {
  char* c_str  = "hello";
  char* c_str1 = "world";
  
  struct STRING str; 
  string$from_c_string(&str, c_str);

  struct STRING str1; 
  string$from_c_string(&str1, c_str1);


  struct LIST   *list = list$make();
  struct STRING *p_str;

  list$append(list, &str);
  list$append(list, &str1);
  s32 loop_count = 0;

  if(!list$empty(list)) {
    struct LIST_ITEM *li = list$first(list);
    do {
      p_str = list$get(li);
      string$null_term(p_str);

      // printf("i: %s\n", string$chars(p_str));

      li = list$next(li);
      loop_count ++;

    } while(list$has_next(li));
  }

  if(loop_count != 2) {
    printf("***FAILED*** did not loop the expected times: 2\n");
    printf("***FAILED*** actually looped: %d\n", loop_count);
    return 1;
  }
  return 0;  
}


