/*
 
   Copyright 2019 RNGHACK

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

// STRING   ~~~~~~~~~~~~~~~~~~~~~~~
struct STRING {
  struct    
  BASE_TYPE  base;     

  struct    
  ARRAY_128  array_128;     

};


void string$init(struct STRING *string) {
  string->array_128.array.capacity     = 128;
  string->array_128.array.size         = 0;
  string->array_128.array.offset_start = 0;
  string->array_128.array.offset_end   = 0;
  string->array_128.array.mark         = 0;
}


struct STRING *struct_STRING() {
  struct STRING *string;

  string = malloc(sizeof(struct STRING));

  string$init(string);

  return string; 
}


struct STRING *string$make() {
  struct STRING *str = struct_STRING();
  string$init(str);
  return str;
}


void string$clear(struct STRING *str) {
  str->array_128.array.size = 0; 
}


u32 string$len(struct STRING *string) {
  return string->array_128.array.size; 
}


u32 string$capacity(struct STRING *string) {
  return string->array_128.array.capacity; 
}


void string$shrink_by(struct STRING *str, u32 amount) {
  if(amount > string$len(str) ) {
    return;
  }
  str->array_128.array.size -= amount; 
}


void string$null_term(struct STRING *string) {
  // make sure there buffer has enough room
 
  u32 str_size = string->array_128.array.size;
  if(str_size < 127) {
    string->array_128.body[str_size] = 0;
  }
}


char* string$chars(struct STRING *string) {
   return (char *)string->array_128.body;
}


char string$last_char(struct STRING *string) {
  u32 str_size = string->array_128.array.size;

  if(str_size == 0) {
    return 0;
  }
  return string->array_128.body[str_size-1];
} 


s32 string$last_index_of_char(struct STRING *string, u8 the_char) {
  u32 str_size = string->array_128.array.size;

  if(str_size == 0) {
    return -1;
  }
  u32 i = str_size - 1;
  bool found = false;

  while(i > 0 && found == false) {
    if(string->array_128.body[i] == the_char) {
      return i;
    } 
    i--;
  } 
  return i; 
}


void string$from_c_string(struct STRING *string, char *c_string) {
  string$init(string);

  u32 char_string_len = strlen(c_string); 
  u32 copy_amount = char_string_len;

  if(copy_amount > 128) {
    copy_amount = 128;
  }
  for(s32 i = 0; i <= copy_amount; i++) {
    string->array_128.body[i] = c_string[i];
  }
  string->array_128.array.size = copy_amount;
}


// TODO write test for below
void string$from_c_string_limit(struct STRING *string, char *c_string, u32 limit) {
  string$init(string);

  u32 char_string_len = strlen(c_string); 
  u32 copy_amount = char_string_len;

  if(copy_amount > 128) {
    copy_amount = 128;
  }
  if(limit < copy_amount) {
    copy_amount = limit;
  }
  for(s32 i = 0; i <= copy_amount; i++) {
    string->array_128.body[i] = c_string[i];
  }
  string->array_128.array.size = copy_amount;
}


char *string$to_c_string(struct STRING *string) {
  string$null_term(string);
  return (char *)string->array_128.body;
}


void string$reverse(struct STRING *string) {
  // abc
  // abcde

  u32 len = string$len(string);

  if(len < 2) {
    return;
  }
  len--;

  u32 i       = 0; 
  char *chars = string$chars(string);
  char x;

  while(i < len) {
    x = chars[i];
    chars[i]   = chars[len];
    chars[len] = x; 
    
    i++;
    len--;
  }
}


bool string$append(struct STRING *str1, struct STRING *str2) {
  u32 str1_len      = string$len(str1);
  u32 str1_capacity = string$capacity(str1);

  u32 str2_len = string$len(str2);

 
  // check that the is enough remove in str1
  if(str1_len + str2_len > str1_capacity) {
    return false;
  }

  // copy the bytes
  for(u32 i = 0; i < str2_len; i++) {
    str1->array_128.body[str1_len + i] = str2->array_128.body[i];
  }
  
  // update the size of str1
  str1->array_128.array.size += str2_len;
  return true;
}



bool string$append_char(struct STRING *string, char c) {
  u32 string_len      = string$len(string);
  u32 string_capacity = string$capacity(string);
 
  // check that the is enough room for the extra char
  if(string_len + 1 > string_capacity) {
    return false;
  }

  // copy the char in
  string->array_128.body[string_len] = c;
  
  // update the size of str1
  string->array_128.array.size++;
  return true;
}


bool string$append_chars(struct STRING *string, char *chars) {
  u32 string_len      = string$len(string);
  u32 string_capacity = string$capacity(string);

  u32 chars_len = strlen(chars);

 
  // check that the is enough room in str1
  if(string_len + chars_len > string_capacity) {
    return false;
  }

  // copy the bytes
  for(u32 i = 0; i < chars_len; i++) {
    string->array_128.body[string_len + i] = chars[i];
  }
  
  // update the size of str1
  string->array_128.array.size += chars_len;
  return true;
}


void string$u32_to_string(struct STRING *string, u32 num) { 
  string$init(string);

  u32 answer;
  u32 val; 

  while((answer = num / 10) > 0) {
    val = num % 10;
    string$append_char(string, val + 48); 
    num = answer;
  }
  
  string$append_char(string, num + 48); 
  string$reverse(string);
}


bool string$append_u32(struct STRING *string, u32 num) { 
  struct STRING buffer;

  string$u32_to_string(&buffer, num);
  
  return string$append(string, &buffer); 
}


void string$append_padding(struct STRING *string, 
		           u8 the_char, 
			   u32 padding) { 
  
  u32 len   = string$len(string); 
  s32 count = padding - len;

  if(count < 1) {
    printf("PPPPPPPPPPPPPPPP %d %d\n", padding, len);
    return;
  } 
 
  if(count + len > string$capacity(string)) {
    printf("QQQQQQQQQQQQQQQ\n");
    return;
  }  

  for(u32 i = 0; i < count; i++) {
    string->array_128.body[len + i] = the_char;  
  } 
  string->array_128.array.size += count;
}


bool string$append_chars_section(struct STRING *string, 
                                 u32 start, u32 count,
				 char *chars) {
  if(count == 0) {
    return true;
  }

  u32 string_len      = string$len(string);
  u32 string_capacity = string$capacity(string);

  if(string_len + count - start > string_capacity) {
    return false;
  } 

  for(u32 i = 0; i < count; i++) {
    string->array_128.body[string_len + i] = chars[start + i];
  } 

  // update the size of str1
  string->array_128.array.size += count - start;
  return true;
}


bool string$copy_append(struct STRING *string, u32 start, u32 count) {
  u32 str_len      = string$len(string);
  u32 str_capacity = string$capacity(string);

  // check that the is enough remove in str1
  if(str_len + count > str_capacity) {
    return false;
  }

  // TODO check copy range is valid
  if(start + count > str_len)  {
    return false;
  }
  
  // copy the bytes
  for(u32 i = 0; i < count; i++) {
    string->array_128.body[str_len + i] = string->array_128.body[start + i];
  }
   
  string->array_128.array.size += count;

  return true;
  
}


void string$copy(struct STRING *src, struct STRING *dest) {
  string$init(dest);
  u32 len = src->array_128.array.size;

  for(u32 i = 0; i < len; i++) {
    dest->array_128.body[i] = src->array_128.body[i]; 
  }

  dest->array_128.array.size = len; 
}


bool string$copy_chars(struct STRING *dest, char *chars, u32 start, u32 count) {
  string$init(dest);
  u32 len       = dest->array_128.array.capacity; 
  u32 chars_len = strlen(chars);

  if(start + count > len) {
    return false;
  }

  for(u32 i = 0; i < count; i++) {
    dest->array_128.body[i] = chars[start + i]; 
  }

  dest->array_128.array.size = count; 

  return true;
}


bool string$copy_string(struct STRING *dest, 
		        struct STRING *src, 
			u32 start, u32 count) {
  string$init(dest);
  u32 len       = dest->array_128.array.capacity; 
  u32 chars_len = string$len(src);

  if(start + count > len) {
    return false;
  }

  for(u32 i = 0; i < count; i++) {
    dest->array_128.body[i] = src->array_128.body[start + i]; 
  }

  dest->array_128.array.size = count; 

  return true;
}


bool string$insert_chars(struct STRING *string, char *chars) {
  u32 string_len      = string$len(string);
  u32 string_capacity = string$capacity(string);

  u32 chars_len = strlen(chars); 
 
  // check that the is enough remove in str1
  if(string_len + chars_len > string_capacity) {
    return false;
  }

  // move the existing chars up
  for(u32 i = string_len; i > 0; i--) {
    string->array_128.body[chars_len + i - 1] = string->array_128.body[i - 1];
  }
  
  // copy in the new chars
  for(u32 i = 0; i < chars_len; i++) {
    string->array_128.body[i] = chars[i];
  }

  // update the size of str1
  string->array_128.array.size += chars_len;
  return true;
}



bool string$replace(struct STRING *string, u32 start, u32 amount, struct STRING *new) {
  u32 old_len = string$len(string);
  u32 string_capacity = string$capacity(string);

/* TODO
  printf("replace \n");
  printf("  start: %d, amount: %d old_len: %d, string_capacitiy: %d\n",
	 start, amount, old_len, string_capacity);
*/
  // validate input
  if(start + amount >=  old_len) {
    return false;
  }

  // check the capacity
  u32 new_len  = string$len(new); 
  u32 new_size = old_len - amount + new_len;

  if(new_size > string_capacity) {
    return false;
  } 

  s32 change       = new_len - amount;
  u32 copy_to      = start + new_len;
  u32 save_amount  = old_len - (start + amount);
/* TODO 
  printf("amount: %d\n", amount);
  printf("change:  %d\n", change);
  printf("copy_to:  %d\n", copy_to);
  printf("save_amount:  %d\n", save_amount);
*/
  if(change > 0) {  // moving to the right 
  // TODO  printf("moving right\n");
    for(u32 i = save_amount; i > 0; i--) {
      string->array_128.body[copy_to + i - 1] = string->array_128.body[start + amount + i - 1];
    } 
  } else {         // moving to the left
    // TODO printf("moving left\n");

    for(u32 i = 0; i < save_amount; i++) {
      string->array_128.body[copy_to + i] = string->array_128.body[start + amount + i];
    } 
  } 

  // copy the data in
  for(u32 i = 0; i < new_len; i++) {
    string->array_128.body[start + i] = new->array_128.body[i];
  } 
  // set the new size
  string->array_128.array.size = new_size;
/*
  printf("-----------\n");
  printf("%s\n", string$to_c_string(string));

  printf("-----------\n");
  */

  return true;
}


u32 string$size(struct STRING *string) {
  return string->array_128.array.size;
}


u8 string$char_at(struct STRING *string, s32 index) {
  u32 str_size = string$size(string);
  
  if(index < 0) {
    return 0;
  }

  if(index >= str_size) {
    return 0;
  } 

  return string->array_128.body[index];
}


bool string$char_before_equals(struct STRING *string, s32 index, char the_char) {
  // check that the index > 0 and < string$size
  u32 str_size = string$size(string);
  index--;


  if(index >= str_size) {
    return false;
  } 
  if(index < 0) {
    return false;
  }

  if(string$char_at(string, index) == the_char) {
    return true;
  }  
  return false;
}


bool string$char_after_equals(struct STRING *string, s32 index, char the_char) {
  // check that the index is less that the last character 
  u32 str_size = string$size(string);
  index++;

  if(index >= str_size) {
    return false;
  } 
  if(index == 0) {
    return false;
  }
  if(string$char_at(string, index) == the_char) {
    return true;
  }  
  return false;  
}


bool string$equals(struct STRING *string1, struct STRING *string2) {
  // check length
  u32 s1_len = string1->array_128.array.size;

  if(s1_len != string2->array_128.array.size) {
    return false;
  }

  // offset TODO: FIX probably need to check how STRING struct is initialised
  //
  /*
  s32 string1_offset = string1->array_128.array.offset_start;
  s32 string2_offset = string2->array_128.array.offset_start;
  */
  s32 string1_offset = 0;
  s32 string2_offset = 0;

  // compare chars
  for(u32 i = 0; i < s1_len; i++) {
    if(string1->array_128.body[i + string1_offset] 
         != string2->array_128.body[i + string2_offset]) {
      return false;
    }
  }

  return true;
}


bool string$equals_chars(struct STRING *string1, char *chars) {
  // TODO write test for this
  // check length
  u32 s1_len = string1->array_128.array.size;
  u32 s2_len = strlen(chars);

  if(s1_len != s2_len) {
    return false;
  }

  // offset TODO: FIX probably need to check how STRING struct is initialised
  //
  /*
  s32 string1_offset = string1->array_128.array.offset_start;
  s32 string2_offset = string2->array_128.array.offset_start;
  */
  s32 string1_offset = 0;
  s32 string2_offset = 0;

  // compare chars
  for(u32 i = 0; i < s1_len; i++) {
    if(string1->array_128.body[i + string1_offset] 
         != chars[i + string2_offset]) {
      return false;
    }
  }

  return true;
}


bool string$char_at_in(struct STRING *string, 
		       struct STRING *chars,
		       s32 index) {

  // get the char and check for index validity
  u8  the_char  = string$char_at(string, index);
  s32 chars_len = string$size(chars);

  if(the_char == 0) {
    return false;
  }

  // loop through each char
  for(s32 i = 0; i < chars_len; i++) {
    if(the_char == string$char_at(chars, i)) {
      return true;
    }  
  } 
  return false;
}

s32 string$offset_start(struct STRING *string) {
  return string->array_128.array.offset_start;
}


s32 string$offset_end(struct STRING *string) {
  return string->array_128.array.offset_end;
}


void string$trim_with_offset(struct STRING *string) {
  struct STRING wss;  // white space char, TODO: make a constant
  char *wsc = " \t";

  string$from_c_string(&wss, wsc); 

  u32  str_len = string$size(string);

  if(str_len == 0) {
    return;
  }

  // search from beginning 
  bool cont = true;
  s32 i = 0;

  while(cont) {
    cont = string$char_at_in(string, &wss, i);

    i++;
    cont = i < str_len && cont; 
  }
  
  string->array_128.array.offset_start = i - 1;

  // search from end 
  i = str_len -1;
  s32 j = 0;
  cont = true;
  while(cont > 0) {
    cont = string$char_at_in(string, &wss, i);

    i--;
    j++;
    cont = i > 0 && cont; 
  }
  string->array_128.array.offset_end = j - 1;
}


s32 string$mark(struct STRING *string) {
 return string->array_128.array.mark;
}


s32 string$index_of(struct STRING *string, u8 c) {
  u32 size = string->array_128.array.size;
  
  if(size < 1) {
    return -1;
  } 

  char *chars = (char *)string->array_128.body;
  s32 i = 0;

  while(size > 0) {
    if(chars[i] == c) {
      return i;
    }

    i++;
    size--;
  }

  return -1;
} 


bool string$start_with_char(struct STRING *str, char the_char) {
  u32 chars_len       = str->array_128.array.size;;

  if(chars_len == 0) {
    return false;
  }

  if(str->array_128.body[0] == the_char) {
    return true;
  }

  return false; 
}


bool string$start_with_chars(struct STRING *str, char *start_chars) {
  u32 chars_len       = str->array_128.array.size;;
  u32 start_chars_len = strlen(start_chars);

  if(start_chars_len > chars_len || chars_len == 0 || start_chars_len == 0) {
    return false;
  }

  u32 i = start_chars_len - 1;

  while(i < start_chars_len) {
    if(str->array_128.body[i] != start_chars[i]) {
      return false;
    }
    i++;
  } 

  return true; 
}


bool string$chars_start_with(char *chars, char *start_chars) {
  u32 chars_len       = strlen(chars);
  u32 start_chars_len = strlen(start_chars);

  if(start_chars_len > chars_len || chars_len == 0 || start_chars_len == 0) {
    return false;
  }

  u32 i = start_chars_len - 1;

  do {
    if(chars[i] != start_chars[i]) {
      return false;
    }
    i--;
  } while(i > 0);

  return true; 
}


s32 string$chars_start_with_ignore_white_space(char *chars, char *start_chars) {
  u32 chars_len       = strlen(chars);
  u32 start_chars_len = strlen(start_chars);
  u32 start_at        = 0 ;

  if(chars_len < 1 || start_chars == 0) {
    return -1;
  } 
  // skip any white space
  u8 c;
  bool cont = true;

  while(cont && start_at < chars_len) {
    c = chars[start_at];

    if(c != ' ' && c != '\t') {
      cont = false;
    }
    start_at ++; 
  } 

  if(start_at > 0) {
    start_at --;
  }

  // check to see if there is still enough room for start_chars 
  if(start_at + start_chars_len > chars_len) {
    return -1;
  }
  u32 i = 0;

  // check the start
  while(i < start_chars_len) {
    if(chars[i + start_at] != start_chars[i]) {
      return -1;
    }
    i++;
  } 
  
  return start_at; 
}



// TODO write test
s32 string$chars_index_of(char *chars, u8 c) {
  u32 size = strlen(chars);
  
  if(size < 1) {
    return -1;
  } 

  s32 i = 0;

  while(size > 0) {
    if(chars[i] == c) {
      return i;
    }

    i++;
    size--;
  }

  return -1;
}



// TODO write test
s32 string$chars_index_of_from(char *chars, u8 c, u32 start_at) {
  u32 size = strlen(chars);
  
  if(size < 1 || start_at >= size) {
    return -1;
  } 

  s32 i = start_at;
  size -= start_at;

  while(size > 0) {
    if(chars[i] == c) {
      return i;
    }

    i++;
    size--;
  }

  return -1;
}


u32 string$chars_to_u32(char *chars, u32 offset) {
  u32 num = 0;
  u32 i = 0;

  char c = chars[i + offset];
 
  while(c >= 48 && c <= 57 && i < 10) {
    if(i > 0) {
      num *= 10; 
    }

    num += c - 48;
     
    i++;
    c = chars[i + offset];
  }
  
  return num;
}


void print(struct STRING *str) {
  string$null_term(str);

  char *chars;
  chars = string$chars(str);

  printf("%s", chars);
}


void println(struct STRING *str) {
  string$null_term(str);

  char *chars;
  chars = string$chars(str);

  printf("%s\n", chars);
}


s32 string$index_of_in_using_offset(struct STRING *string, 
				    struct STRING *chars,
				              s32 index) {

  u32 size = string->array_128.array.offset_end;
  s32    i = string->array_128.array.offset_start;

  printf("offset start: %d", i );
  printf("offset end:   %d", size );
  
  if(size < 1) {
    return -1;
  } 

  printf("size: %d\n", size);

  printf("offset_start: %d\n", size);

  while(i < size) {
    bool is_found = string$char_at_in(string, chars, i);

    if(is_found == true) {
      // TODO: ajust for offset 
      return i;
    }

    i++;
  }

  return -1;
}


// TODO bool for split on strings
// TODO refactor process line to use this
// TODO add safety so that we don't overflow on lines
// TODO add tests
u32 string$split_to_array(char *line_chars, struct STRING *line[]) {
  u32 line_len = strlen(line_chars);
  if(line_len == 0) {
    return 0;
  } 

  u32    index              = 0;
  u32    i                  = 0;
  bool   started            = false;
  bool   string_mode        = false;
  struct STRING *working_str = string$make(); 

  // Split line into strings 
  while(i < line_len) {
    if(line_chars[i] == '\n') {
      // ignore
    } else if(string_mode == false 
	       && (line_chars[i] == ' ' || line_chars[i] == '\t')) {

      if(started != false) {
        // add the string to the array list
	line[index] = working_str;
	index++;
	working_str = string$make();
	started = false;
      }
    } else if(line_chars[i] == '\"') {
      if(string_mode == false) {
        string_mode = true; 
      } else if(string_mode == true) {
        string_mode = false; 
        string$append_char(working_str, line_chars[i]);
      }
    } else {
      // Copy char
      started = true;
      string$append_char(working_str, line_chars[i]);
    }
    i++;
  }
  if(started) {
    line[index] = working_str;
    index++;
  }
  return index; // index is the word count;
}


// TODO bool for split on strings
// TODO refactor process line to use this, do i mean in wax?
// TODO add safety so that we don't overflow on lines
// TODO improve tests
u32 string$split_to_array_with_quoted(char *line_chars, struct STRING **tokens, u32 max_tokens) {
  u32 line_len = strlen(line_chars);
  if(line_len == 0) {
    return 0;
  } 

  u32    index              = 0;
  u32    i                  = 0;
  bool   started            = false;
  bool   string_mode        = false;
  struct STRING *working_str = string$make(); 

  // Split line into strings 
  while(i < line_len) {
    if(line_chars[i] == '\n') {
      // ignore
    } else if(string_mode == false 
	       && (line_chars[i] == ' ' || line_chars[i] == '\t')) {

      if(started != false) {
        // add the string to the array list
	tokens[index] = working_str;
	index++;
	working_str = string$make();
	started = false;
      }
    } else if(line_chars[i] == '\"') {
      if(string_mode == false) {
        string_mode = true; 
      } else if(string_mode == true) {
        string_mode = false; 
      }
    } else {
      // Copy char
      started = true;
      string$append_char(working_str, line_chars[i]);
    }
    i++;
  }
  if(started) {
    tokens[index] = working_str;
    index++;
  }
  return index; // index is the word count;
}


// replace escaped characters with there actual characters
void string$escaped_to_special(struct STRING *str) {
  u32 str_size = str->array_128.array.size;

  if(str_size == 0) {
    return;
  }

  // reform array based on copy by appending and replacing

  // create a copy of the existing array
  u8 old_chars[str->array_128.array.capacity];
  memcpy(old_chars, str->array_128.body, str->array_128.array.size);

  u32  i          = 0;
  bool found      = false;
  u32  append_pos = 0;

  while(i < str_size) {
    if(old_chars[i] == '\\' && (i+1 < str_size))  {
      if(old_chars[i+1] == '\\') {
        str->array_128.body[append_pos] = '\\';
        append_pos++;
        i++;
      } else if(old_chars[i+1] == 'n') {
        str->array_128.body[append_pos] = '\n';
        append_pos++;
        i++;
      } else if(old_chars[i+1] == 't') {
        str->array_128.body[append_pos] = '\t';
        append_pos++;
        i++;
      } else if(old_chars[i+1] == 'r') {
        str->array_128.body[append_pos] = '\r';
        append_pos++;
        i++;
      } else {
        str->array_128.body[append_pos]   = old_chars[i];
        str->array_128.body[append_pos+1] = old_chars[i+1];
        //TODO Bug fix and test bug
        append_pos+=2;
        i++;
      } 
    } else {
      str->array_128.body[append_pos] = old_chars[i];
      append_pos++;
    }
    i++;
  } 

  // set the strings new size
  str->array_128.array.size = append_pos;
}


// TESTS --------------------------------------


u32 string_test$escaped_to_special() {
  struct STRING str;
  string$init(&str);

  string$append_chars(&str, "hello\\nworld");
  printf("@before: %s\n", string$to_c_string(&str));

  string$escaped_to_special(&str);

  printf("@after: %s\n", string$to_c_string(&str));

  // TODO add compare test

  return 0;
}


// TODO fix this test
u32 string_test$split_to_array_with_quoted() {
   char *somechars = "\"hello this is\" some chars";

   u32 max_tokens = 10;
   struct STRING **tokens = malloc(max_tokens * sizeof(struct STRING *));

   
   u32 token_count = string$split_to_array_with_quoted(somechars, tokens, max_tokens);
   printf("%d tokens found\n", token_count);
   
   // TODO compare that the strings found are the correct ones
   u32 i = 0;
   while(i < token_count) {
     printf("%s\n", string$to_c_string(tokens[i]));
     i++;
   }

   // TODO free the memory 

   if(token_count != 3) {
     return 1;
   }
   return 0; 
}


u32 string_test$index_of_in_using_offset() { 

  char *cstring = "  sub a, b";
  struct STRING str;
  string$from_c_string(&str, cstring);

  char *cchars = " ,\t";
  struct STRING chars;
  string$from_c_string(&chars, cchars);

  string$trim_with_offset(&str);

  s32 result = string$index_of_in_using_offset(&str, &chars, 0);

  if(result != 3) {
    printf("***FAILED*** result: %d\n", result);
    return 1;
  } 
	/*
  result = string$index_of(&str, 'Q');

  if(result != -1) {
    printf("***FAILED*** should not exist.\n");
    return;
  }
  */

  return 0; 
}


u32 string_test$append() {
  

  char *str1 = "hello";
  char *str2 =   " world";
  char *result = "hello world";

  struct STRING str_1;
  string$from_c_string(&str_1, str1);
  struct STRING str_2;
  string$from_c_string(&str_2, str2);
  struct STRING str_result;
  string$from_c_string(&str_result, result);

  bool success = string$append(&str_1, &str_2);

  if(success == false) {
    printf("***FAILED*** (append returned false)\n");

    printf("str1_len: %d", string$len(&str_1));
    printf("str1_capacity: %d", string$capacity(&str_1));
    printf("str2_len: %d", string$len(&str_2));
    
    return 1;
  }

  if(!string$equals(&str_1, &str_result)) {
    printf("***FAILED*** (appended string not equal to expected result)\n");
    println(&str_1);

    printf("str1_len: %d", string$len(&str_1));
    printf("str1_capacity: %d", string$capacity(&str_1));
    printf("str2_len: %d", string$len(&str_2)); 

    return 1;
  }
  return 0;
}


u32 string_test$copy() {

  char *src_c = "copy this string";

  struct STRING src;
  string$from_c_string(&src, src_c);

  struct STRING dest;
  string$copy(&src, &dest);

  char *expected_c = "copy this string";
  struct STRING expected; 

  string$from_c_string(&expected, expected_c);
  
  if(!string$equals(&expected, &dest)) {
    printf("***FAILED*** (copied string not equal to expected result, did not expect: %s\n", 
	   string$to_c_string(&dest));

    return 1;
  }

  return 0;
}


u32 string_test$copy_chars() {


  struct STRING dest;

  char *chars = "copy this string";
  string$copy_chars(&dest, chars, 5, 11);

  char *expected_c = "this string";
  struct STRING expected; 

  string$from_c_string(&expected, expected_c);
  
  if(!string$equals(&expected, &dest)) {
    printf("***FAILED*** (copied string not equal to expected result, did not expect: %s\n", 
	   string$to_c_string(&dest));

    return 1;
    
  }

  return 0;

}


u32 string_test$from_c_string() {

  char *cstring = "hello";
  struct STRING str;

  string$from_c_string(&str, cstring);
  string$null_term(&str);

  if(str.array_128.array.size != strlen(cstring)) {
    printf("***FAILED*** (strings not the same length)\n");
    return 1;
  }

  if(cstring[0] != 'h') {
    printf("***FAILED*** (char[0] doesn't match)\n");
    return 1;
  }
  if(cstring[1] != 'e') {
    printf("***FAILED*** (char[1] doesn't match)\n");
    return 1;
  }
  if(cstring[2] != 'l') {
    printf("***FAILED*** (char[2] doesn't match)\n");
    return 1;
  }
  if(cstring[3] != 'l') {
    printf("***FAILED*** (char[3] doesn't match)\n");
    return 1;
  }
  if(cstring[4] != 'o') {
    printf("***FAILED*** (char[4] doesn't match)\n");
    return 1;
  }
  return 0;
}


u32 string_test$char_before_equals() {
  char *cstring = "add : sum s32";
  struct STRING str;

  bool found = string$char_before_equals(&str, 5, ':');

  if(found == true) {
    return 0;
  } else {
    printf("Failed.\n");   
    return 1;
  } 
}

u32 string_test$char_after_equals() {
  char *cstring = "add : sum s32";
  struct STRING str;
  string$from_c_string(&str, cstring);

  bool found = string$char_after_equals(&str, 4, ' ');

  if(found == true) {
    return 1;
  } else {
    printf("Failed.\n");   
    return 1;
  }
  return 0;
}


u32 string_test$index_of() {

  char *cstring = "add : sum s32";
  struct STRING str;
  string$from_c_string(&str, cstring);

  s32 result = string$index_of(&str, ':');

  if(result != 4) {
    printf("***FAILED*** cannot find : char\n");
    return 1;
  }

  result = string$index_of(&str, 'Q');

  if(result != -1) {
    printf("***FAILED*** should not exist.\n");
    return 1;
  }

  return 0;
}


u32 string_test$chars_index_of_from() {

  char *cstring = "src/wax/wax.c:1402:49:";

  s32 result = string$chars_index_of_from(cstring, ':', 14);

  if(result != 18) {
    printf("***FAILED*** cannot find : char\n");
    return 1;
  }

  return 0;
}


u32 string_test$char_at_in() {

  char *cstring = "add : sum s32";
  char *cchars =   "bcd";
  struct STRING str;
  string$from_c_string(&str, cstring);
  struct STRING chars;
  string$from_c_string(&chars, cchars);

  bool result = string$char_at_in(&str, &chars, 1);

  if(result != true) {
    printf("***FAILED*** cannot find the chars\n");
    return 1;
  }

  result = string$char_at_in(&str, &chars, 3);

  if(result != false) {
    printf("***FAILED*** should not exist.\n");
    return 1;
  }

  return 0;
}


u32 string_test$chars_start_with() {

  char *cstring = "src/wax/wax.c:1402:49:";
  char *cstarts = "src/wax/wax.c";

  if(!string$chars_start_with(cstring, cstarts)) {
    printf("***FAILED*** expect to find %s at the beginning of %s\n", cstarts, cstring);
    return 1;
  }

  return 0;
}


u32 string_test$chars_start_with_ignore_white_space() {

  char *cstring = "   src/wax/wax.c:1402:49:";
  char *cstarts = "src/wax/wax.c";

  s32 result = string$chars_start_with_ignore_white_space(cstring, cstarts);

  if(result < 0) {
    printf("***FAILED*** expect to find %s at the beginning of %s\n", cstarts, cstring);
    return 1;
  }

  return 0;
}


u32 string_test$equals() {

  char *str1 = "abcdef";
  char *str2 = "abcdef";
  char *str3 = "abcdeg";

  struct STRING str_1;
  string$from_c_string(&str_1, str1);
  struct STRING str_2;
  string$from_c_string(&str_2, str2);
  struct STRING str_3;
  string$from_c_string(&str_3, str3);

  bool result = string$equals(&str_1, &str_2);

  if(result != true) {
    printf("***FAILED*** Strings weren't equal\n");
    println(&str_1);
    println(&str_2);
    return 1;
  }

  result = string$equals(&str_2, &str_3);

  if(result != false) {
    printf("***FAILED*** Strings should not be equal.\n");
    println(&str_2);
    println(&str_3);
    return 1;
  }

  return 0;
}


u32 string_test$trim_with_offset() {

  char *cstring = "\t .\t ";
  struct STRING str;
  string$from_c_string(&str, cstring);

  string$trim_with_offset(&str);

  s32 os = string$offset_start(&str);
  s32 oe = string$offset_end(&str);

  if(os != 2) {
    printf("***FAILED*** offset start incorrect\n");
    return 1;
  }
  if(oe != 2) {
    printf("***FAILED*** offset end incorrect\n");
    return 1;
  }

  cstring = "abc";
  string$from_c_string(&str, cstring);

  string$trim_with_offset(&str);

  os = string$offset_start(&str);
  oe = string$offset_end(&str);

  if(os != 0) {
    printf("***FAILED*** nothing to trim offset start incorrect\n");
    printf("offset start %d\n", os);
    return 1;
  }
  if(oe != 0) {
    printf("***FAILED*** nothing to trim offset end incorrect\n");
    printf("offset end %d\n", oe);
    return 1;
  }

  return 0;
}


u32 string_test$insert_chars() {

  char *cstring = "world";
  struct STRING str;
  string$from_c_string(&str, cstring);

  char *new_chars = "hello ";

  char *c_result = "hello world";
  struct STRING result;
  
  string$from_c_string(&result, c_result);
  string$insert_chars(&str, new_chars);

  if(string$equals(&str, &result) != true) {
    printf("***FAILED*** result no expected, got: ");   
    printf("[");   
    print(&str);
    printf("]");   
    printf(" instead of [hello world]");
    return 1; 
  } 

  return 0;
}


u32 string_test$append_chars() {

  char *cstring = "hello";
  struct STRING str;
  string$from_c_string(&str, cstring);

  char *new_chars = " world";

  char *c_result = "hello world";
  struct STRING result;
  
  string$from_c_string(&result, c_result);
  string$append_chars(&str, new_chars);

  if(!string$equals(&str, &result)) {
    printf("***FAILED*** result no expected, got: ");   
    printf("[");   
    print(&str);
    printf("]");   
    printf(" instead of [hello world]");
    return 1; 
  } 

  return 0;  
}


u32 string_test$copy_append() {

  char *c_test = "hello world";
  struct STRING test;
  string$from_c_string(&test, c_test);

  char *c_result = "hello worldworld";
  struct STRING result;
  string$from_c_string(&result, c_result);

  string$copy_append(&test, 6, 5);

  if(!string$equals(&test, &result)) {
    printf("***FAILED*** result no expected, got: ");   
    printf("[");   
    print(&result);
    printf("]");   
    printf(" instead of [hello worldworld]\n");
    return 1; 
  } 

  return 0;  
}


u32 string_test$chars_to_u32() {

  char *chars = "hello 142 world";
  u32  result = string$chars_to_u32(chars, 6);

  if(result != 142) {
    printf("*** FAILED *** Expected 142 but got: %d\n", result);
    return 1;
  }

  return 0;
}
  

u32 string_test$reverse() {

  char *c_input  = "dlrow";
  char *c_expected = "world";

  struct STRING input;
  string$from_c_string(&input, c_input);

  struct STRING expected;
  string$from_c_string(&expected, c_expected); 

  string$reverse(&input);

  if(!string$equals(&input, &expected)) {
    printf("*** FAILED *** Expected: world but got: %s\n", string$to_c_string(&input));
    return 1;
  } 

  return 0;
}


u32 string_test$u32_to_string() {

  u32 num = 4231;
  char *c_result = "4231";

  struct STRING result;
  string$from_c_string(&result, c_result);

  struct STRING str_num;
  string$u32_to_string(&str_num, num);

  if(!string$equals(&result, &str_num)) {
    printf("*** FAILED *** Expected: 4231 but got: %s\n", string$to_c_string(&str_num));
    return 1;
  }

  return 0;  
}


u32 string_test$replace() {
  // TODO add more tests for replacing empty strings


  // test string with replace string being longer than replace amount
  char *chars1_c = "abc1234def";
  struct STRING str1;
  string$from_c_string(&str1, chars1_c);

  char *new1_c = "567890";
  struct STRING new1;
  string$from_c_string(&new1, new1_c);

  char *result1_c = "abc567890def";
  struct STRING result1;
  string$from_c_string(&result1, result1_c);
  

   string$replace(&str1, 3, 4, &new1);

  if(!string$equals(&str1, &result1)) {

    printf("\n***FAILED*** Expected: %s but got: %s\n", string$to_c_string(&result1), 
		                                        string$to_c_string(&str1));
    return 1;
  }

  // test string with replace string being shorter than replace amount
 
  char *chars2_c = "abc1234def";
  struct STRING str2;
  string$from_c_string(&str2, chars2_c);

  char *new2_c = "PQ";
  struct STRING new2;
  string$from_c_string(&new2, new2_c);

  char *result2_c = "abcPQdef";
  struct STRING result2;
  string$from_c_string(&result2, result2_c);

  string$replace(&str2, 3, 4, &new2);

  if(!string$equals(&str2, &result2)) {

    printf("\n***FAILED*** Expected: %s but got: %s\n", string$to_c_string(&result2), 
		                                        string$to_c_string(&str2));
    return 1;
  }

  // TODO test string with replace string being the same as the replace amount

  // TODO test string with replace string empty (amount is 0) 
  
  // TODO test string with replace string with an empty string 
 
  return 0;  
}


void string$free(struct STRING *string) {
}
