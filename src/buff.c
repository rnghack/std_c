/*
 
   Copyright 2021 RNGHACK

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/


struct BUFF_FIXED {
  u32  max;
  u32  used;
  u32  data_width;
  void *data; 
};


struct BUFF_FIXED* buff$make_fixed(u32 max_size) {
  // create the struct
  struct BUFF_FIXED *buff = malloc(sizeof(struct BUFF_FIXED));
  
  // create the buffer a
  buff->data = malloc(max_size);
  buff->max  = max_size;
  
  // return  
  return buff;
} 


void buff$del_fixed(struct BUFF_FIXED *buff) {
  free(buff->data);
  free(buff);
} 


u32 buff_test$make_del_fixed() {
  struct BUFF_FIXED *buff = buff$make_fixed(1024);
  buff$del_fixed(buff);
  return 0; 
}
