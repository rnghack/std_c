FILE *file$open_r(char *path) {
  FILE *fp = fopen(path, "r");
  return fp;
}


FILE *file$open_w(char *path) {
  FILE *fp = fopen(path, "w");
  return fp;
} 


u32 file$close(FILE *file_handle) {
  fclose(file_handle);
  return 0;
}

// TODO complete
u32 file$read(u32 file_handle, char* buffer, s32 read_size) {
  // read = fread(buffer, read_size, 1, file_handle);
  return 0;
}


// TODO complete
u32 file$write(u32 file_handle, struct ARRAY_128 *buffer) {
  return 0;
}


u32 file$cat(char *path) { 
  FILE *fp = file$open_r(path);

  if(fp == 0) {
    return 0; // TODO should we return -1 here for an error???
  }

  char *line   = NULL;
  size_t len   = 10;
  ssize_t read; 

  while ((read = getline(&line, &len, fp)) != -1) {
    printf("%s", line);
  } 

  return file$close(fp); 
}


bool file$exists(char *path) {
  if(access(path, F_OK) != -1) {
    return true;
  } 
  return false; 
} 
