/*
 
   Copyright 2021 RNGHACK

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/ 


u32 sys$exec(struct STRING *path, struct STRING *command, struct BUFF_FIXED *result) {
    u32 BUFSIZE = 1024;
    // TODO check buff->max >= BUFFSIZE
    //

    struct STRING shell_command;
    string$init(&shell_command);

    string$append_chars(&shell_command, "cd ");
    string$append(&shell_command,       path);
    string$append_chars(&shell_command, " && ");
    string$append(&shell_command,       command);

    result->used    = 0;
    ((char *)result->data)[0] = NULL;

    // printf("Shell command: %s\n", string$to_c_string(&shell_command));

    char buf[BUFSIZE];
    FILE *fp;

    if ((fp = popen(string$to_c_string(&shell_command), "r")) == NULL) {
        printf("Error opening pipe!\n");
        return -1;
    }

    // TODO make the buff copy safer
    while (fgets(buf, BUFSIZE, fp) != NULL && result->used < result->max -1) {
      // copy to buffer
      int index = 0;
      int start_at = result->used;
      char c    = 0; 
/*
      if(result->used > 0) {
        ((char *)result->data)[start_at + index] = '\n';
        index++;
      }
      */
      // TODO make the buff copy safer
      while(buf[index] != (char)NULL) {
        ((char *)result->data)[start_at + index] = buf[index];
        index++;
      }
      result->used += index-1;
    }

    u32 exit_code = pclose(fp);

    // add null terminator
    result->used ++;
    ((char *)result->data)[result->used] = NULL;

    return exit_code;
}


// Wall clock time
u64 sys$millis() {
  struct timespec time;
  clock_gettime(CLOCK_REALTIME, &time);

  u64 s = time.tv_sec;
  u64 ms = round(time.tv_nsec / 1.0e6); // Convert nanoseconds to milliseconds

  if (ms > 999) {
    s++;
    ms = 0;
  }

  u64 millis = s * 1000 + ms;

  return millis;
}


// For stable measurement of elapsed time
u64 sys$timer() {
  struct timespec time;
  
  clock_gettime(CLOCK_MONOTONIC, &time);

  u64 s = time.tv_sec;
  u64 ms = round(time.tv_nsec / 1.0e6); // Convert nanoseconds to milliseconds

  if (ms > 999) {
    s++;
    ms = 0;
  }

  u64 millis = s * 1000 + ms;

  return millis;
}


u32 sys_test$exec() {
  struct STRING *path        = string$make();
  struct STRING *command     = string$make();
  struct BUFF_FIXED *result  = buff$make_fixed(10000);

  string$append_chars(path,    "~/work");
  string$append_chars(command, "ls -lah");
  u32 exit_code = sys$exec(path, command, result);

  // printf("exit_code: %d,\n%s", exit_code, result->data);

  if(exit_code == 0) return 0;
  return 1;
}


u32 sys_test$millis() {
  u64 millis = sys$millis();
  // printf("mills %lu\n", millis);
  return 0;
}

u32 sys_test$timer() {
  u64 millis = sys$timer();
  // printf("mills %lu\n", millis);
  return 0;
}
