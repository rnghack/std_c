/*
 
   Copyright 2019 RNGHACK

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

#define _XOPEN_SOURCE 700 

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <math.h>



// BASE     ~~~~~~~~~~~~~~~~~~~~~~~
//
#define true  1
#define false 0

#define bool  char

#define s8  char
#define u8  unsigned char

#define s32  int
#define u32  unsigned int

#define s64  long
#define u64  unsigned long

#define line_type_not_found -1

#define line_type_fn_header  1 

#define line_type_parameter  2


struct BASE_TYPE {
  u32  type;
  u64  offset;
};

// ARRAY    ~~~~~~~~~~~~~~~~~~~~~~~
struct ARRAY_TYPE {
  struct    
  BASE_TYPE  base;     
  u32        width;  
  u32        capacity;
  u32        size;   
  u32        offset_start;
  u32        offset_end;
  u32        mark;
};


struct ARRAY_128 {
  struct    
  ARRAY_TYPE  array;     
  u8          body[128];
};

